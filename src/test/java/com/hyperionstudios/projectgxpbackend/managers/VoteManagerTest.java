/*
 * Copyright (C) 2014 Dylan.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package com.hyperionstudios.projectgxpbackend.managers;

import com.hyperionstudios.projectgxpbackend.Main;
import java.util.Calendar;
import java.util.GregorianCalendar;
import junit.framework.TestCase;

/**
 *
 * @author Dylan
 */
public class VoteManagerTest extends TestCase {
    
    public VoteManagerTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        Main.config.Load();
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of hasMonthElapsed method, of class VoteManager.
     */
    public void testHasMonthElapsed() {
        System.out.println("hasMonthElapsed");
        VoteManager instance = new VoteManager();
        
        Calendar calendar;
         
        //1 days passed
        calendar = GregorianCalendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Main.config.getOptions().setLastVotePurge(calendar.getTime());
        assertTrue("A month has NOT passed, but reported it has!", !instance.hasMonthElapsed());
        
        //one month passed
        calendar = GregorianCalendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        Main.config.getOptions().setLastVotePurge(calendar.getTime());
        assertTrue("A month has passed, but reported it has not!", instance.hasMonthElapsed());
        
        //five months passed
        calendar = GregorianCalendar.getInstance();
        calendar.add(Calendar.MONTH, -5);
        Main.config.getOptions().setLastVotePurge(calendar.getTime());
        assertTrue("A month has passed, but reported it has not!", instance.hasMonthElapsed());
       
    }

}
