/*
 * Copyright (C) 2014 Dylan.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package com.hyperionstudios.projectgxpbackend.entities;

import com.hyperionstudios.projectgxpbackend.Main;
import java.util.Calendar;
import java.util.GregorianCalendar;
import junit.framework.TestCase;

/**
 * Tests for {@link Server}.
 * 
 * @author Pickle <curtisdhi@gmail.com>
 */
public class ServerTest extends TestCase {
    
    public ServerTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Main.config.Load();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    /**
     * Test of getUptimePercentage method, of class Server.
     */
    public void testGetUptimePercentage() {
        System.out.println("getUptimePercentage");
        Server instance = new Server();
        float result;
        
        //-10% uptime
        instance.setUptime(-9);
        instance.setDowntime(100);
        result = instance.getUptimePercentage();
        assertTrue("uptime percentange is out of range: " + result, 0 <= result);
        
        //0% uptime
        instance.setUptime(0);
        instance.setDowntime(100);
        result = instance.getUptimePercentage();
        assertTrue("uptime percentange is out of range: " + result, 
                0 <= result && result <= 1);
        //20% uptime
        instance.setUptime(25);
        instance.setDowntime(100);
        result = instance.getUptimePercentage();
        assertTrue("uptime percentange is not 20%: " + result, result == 0.2f);
        //50% uptime
        instance.setUptime(100);
        instance.setDowntime(100);
        result = instance.getUptimePercentage();
        assertTrue("uptime percentange is not 50%: " + result, result == 0.5f);
        //100% uptime
        instance.setUptime(0);
        instance.setDowntime(0);
        result = instance.getUptimePercentage();
        assertTrue("uptime percentange is out of range: " + result, result <= 1);
        //110% uptime (never can be such thing)
        instance.setUptime(-1100);
        instance.setDowntime(100);
        result = instance.getUptimePercentage();
        assertTrue("uptime percentange is out of range: " + result, result <= 1);
    }

    /**
     * Test of isNew method, of class Server.
     */
    public void testIsNew() {
        System.out.println("isNew");
        
        Server instance = new Server();
        
        //right now
        Calendar calendar = GregorianCalendar.getInstance();
        instance.setCreationDate(calendar.getTime());
        assertTrue("Server is suppose to be NEW, but wasn't.", instance.isNew());
        //yesterday
        calendar = GregorianCalendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        instance.setCreationDate(calendar.getTime());
        assertTrue("Server is suppose to be NEW, but wasn't.", instance.isNew());
        //7days pior
        calendar = GregorianCalendar.getInstance();
        calendar.add(Calendar.DATE, -7);
        calendar.add(Calendar.MINUTE, -1);
        instance.setCreationDate(calendar.getTime());
        assertTrue("Server is suppose to be OLD, but wasn't.", !instance.isNew()); //not new
        //30days pior
        calendar = GregorianCalendar.getInstance();
        calendar.add(Calendar.DATE, -30);
        instance.setCreationDate(calendar.getTime());
        assertTrue("Server is suppose to be OLD, but wasn't.", !instance.isNew()); //not new
    }

    /**
     * Test of isBeyondAcceptableUptime method, of class Server.
     */
    public void testIsBeyondAcceptableUptime() {
        System.out.println("isBeyondAcceptableUptime");
        float acceptableUptime = 0.2f;
        Server instance = new Server();
        
        //0% uptime
        instance.setUptime(0);
        instance.setDowntime(100);
        assertTrue("Server is suppose to be BEYOND uptime threshold, but wasn't: "+ instance.getUptimePercentage(), 
                instance.isBeyondAcceptableUptime(acceptableUptime));
        
        //15% uptime
        instance.setUptime(17);
        instance.setDowntime(100);
        assertTrue("Server is suppose to be BEYOND uptime threshold, but wasn't: "+ instance.getUptimePercentage(),
                instance.isBeyondAcceptableUptime(acceptableUptime));
        
        //20% uptime
        instance.setUptime(25);
        instance.setDowntime(100);
        assertTrue("Server is suppose to be WITHIN uptime threshold, but wasn't: "+ instance.getUptimePercentage(),
                !instance.isBeyondAcceptableUptime(acceptableUptime));

        //70% uptime
        instance.setUptime(233);
        instance.setDowntime(100);
        assertTrue("Server is suppose to be WITHIN uptime threshold, but wasn't: "+ instance.getUptimePercentage(),
                !instance.isBeyondAcceptableUptime(acceptableUptime));
        
        //100% uptime (can never really exist)
        instance.setUptime(100);
        instance.setDowntime(0);
        assertTrue("Server is suppose to be WITHIN uptime threshold, but wasn't: "+ instance.getUptimePercentage(),
                !instance.isBeyondAcceptableUptime(acceptableUptime));
    }
    
}
