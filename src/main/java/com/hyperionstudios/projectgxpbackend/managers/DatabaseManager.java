/*
 * The MIT License
 *
 * Copyright (C) 2014 Pickle <curtisdhi@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hyperionstudios.projectgxpbackend.managers;

import com.hyperionstudios.projectgxpbackend.Main;
import com.hyperionstudios.projectgxpbackend.entities.Server;
import com.hyperionstudios.projectgxpbackend.entities.ServerType;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.db.MysqlDatabaseType;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.sql.SQLException;

/**
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
public class DatabaseManager {
    
    protected JdbcPooledConnectionSource sqlConnection = null;
    protected Dao<Server, Integer> serverDoa;
    protected Dao<ServerType, Integer> serverTypeDoa;
    
    protected final int MaxTimeout = 300000; //only keep the connection open for 5 minutes.

    public DatabaseManager() throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("jdbc:mysql://")
                .append(Main.config.getOptions().getSqlHost())
                .append(":")
                .append(Main.config.getOptions().getSqlPort())
                .append("/")
                .append(Main.config.getOptions().getSqlDatabaseName());
        String url = sb.toString();
 
        String username = Main.config.getOptions().getSqlUsername();
        String password = Main.config.getOptions().getSqlPassword();
        
        try {
            sqlConnection = new JdbcPooledConnectionSource(url, username, password, new MysqlDatabaseType());
            sqlConnection.setMaxConnectionAgeMillis(MaxTimeout);

            serverDoa = DaoManager.createDao(sqlConnection, Server.class);
            serverTypeDoa = DaoManager.createDao(sqlConnection, ServerType.class);
        } catch (SQLException ex) {
            Main.log.fatal("Failed to connected to database!!", ex);
            if (sqlConnection != null) {
                sqlConnection.closeQuietly();
            }
            throw ex;
        } 
        
    }
    
    public JdbcPooledConnectionSource getPooledConnectionSource() {
        return sqlConnection;
    }
    
    public Dao<Server, Integer> getServerDoa() {
        return serverDoa;
    }
    public Dao<ServerType, Integer> getServerTypeDoa() {
        return serverTypeDoa;
    }
    
}
