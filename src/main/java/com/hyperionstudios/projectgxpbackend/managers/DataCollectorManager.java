/*
 * The MIT License
 *
 * Copyright (C) 2014 Pickle <curtisdhi@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.hyperionstudios.projectgxpbackend.managers;

import com.hyperionstudios.projectgxpbackend.managers.DatabaseManager;
import com.hyperionstudios.projectgxpbackend.Main;
import com.hyperionstudios.projectgxpbackend.dataCollectors.DataCollectorFactory;
import com.hyperionstudios.projectgxpbackend.dataCollectors.ServerDataCollector;
import com.hyperionstudios.projectgxpbackend.entities.Server;
import com.hyperionstudios.projectgxpbackend.entities.ServerType;
import com.hyperionstudios.projectgxpbackend.entities.ServerTypes.Types;
import com.j256.ormlite.stmt.QueryBuilder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
public class DataCollectorManager implements Runnable {

    protected final ArrayList<ServerDataCollector> dataCollectors;
    protected final HashMap<Types, Integer> dCollectorsCount;
    
    protected final Thread thread;
    protected final DataCollectorFactory dataCollectorFactory;
    
    protected final DatabaseManager databaseManager;
    
    protected long sqlLimit = 250;
 
    private long maxExecutionTime = 300000L; //5 minutes
    
    public DataCollectorManager() throws SQLException {
        maxExecutionTime = Main.config.getOptions().getDCManagerThreadMaxExecutionTime();
        
        dataCollectorFactory = new DataCollectorFactory(this);
        dataCollectors = new ArrayList<>();
        
        Types[] types = Types.values();
        dCollectorsCount = new HashMap<>(types.length);
        for (Types type : types) {
            dCollectorsCount.put(type, 0);
        }
        
        sqlLimit = Main.config.getOptions().getServersPerThead();
        
        try {
            databaseManager = new DatabaseManager();
        }
        catch (SQLException ex) {
            throw ex;
        }
        
        thread = new Thread(this);
        thread.setName("DCManager");
    }

    @Override
    public void run() {
        while (!thread.isInterrupted()) {
            Date time = new Date();       
            
            Main.log.debug("Datacollectors operating: "+dataCollectors.size());
            
            /*
            * Add datacollectors for each "page" to async mantain a list.
            */
            for (Types type : Types.values()) {
                long count = countServersOfType(type);
                int dCollectorCount = dCollectorsCount.get(type);
                //find how many datacollectors we need to create.
                //Convert sqlLimit to double otherwise integer arithmetic will happen (and not what we want)
                int newCollectorCount = (int)Math.ceil(count / (double)sqlLimit);
                dCollectorsCount.put(type, newCollectorCount);
                
                Main.log.debug(type.name() +" Counted: "+ count);
                
                if (dCollectorCount < newCollectorCount) {
                    int countDiff = newCollectorCount - dCollectorCount;
                    int c = 0;
                    if (dCollectorCount != 0) {
                        //if the previous counter was not 0, then this is not the first time we populated the list.
                        //So we need to start one extra ahead, so we don't have two collectors processing the same data
                        c = 1;
                    }

                    for (int i = c; i < countDiff; i++) {
                        
                        ServerDataCollector dataCollector = dataCollectorFactory.createDataCollector(
                                type, sqlLimit, dCollectorCount + i);
                        if (dataCollector != null) {
                            Main.log.debug("New datacollector!");
                            //Make sure datacollector isn't null because the factory may return one null.
                            dataCollector.getThread().start();
                            dataCollectors.add(dataCollector);
                        }
                    }
                }
                
               
            }
            
            cleanUpCollectors();

            //Let more important threads execute.
            long diffTime = time.getTime() - new Date().getTime();
            long sleepTime = maxExecutionTime - diffTime;
            try {
                Thread.sleep(sleepTime);
            }
            catch (InterruptedException ex) {
                //Main.log.error(null, ex);
                thread.interrupt();
            }
        
        }
        Main.log.debug("Stopping data collector manager "+ thread.getName());
        
        //interrupt and join the datacollector threads
        for (ServerDataCollector sdc : dataCollectors) {
            sdc.getThread().interrupt();
            try {
                sdc.getThread().join();
            } catch (InterruptedException ex) {
                thread.interrupt();
            }
        }
    }
    
    public void start() {
        thread.start();
        for (ServerDataCollector sdc : dataCollectors) {
            sdc.getThread().start();
        }
    }
    
    public void stop() {
        thread.interrupt();
    }
    
    public Thread getThread() {
        return thread;
    }
    
    public DatabaseManager getDBManager() {
        return databaseManager;
    }
    
    public void removeDataCollector(ServerDataCollector dataCollector) {
        Types type = dataCollector.getType();
        dataCollector.getThread().interrupt();
        dCollectorsCount.put(type, dCollectorsCount.get(type) - 1);
        dataCollectors.remove(dataCollector);
    }
    
    /*
    * count how many servers entries there are.
    * @return long
    */
    public long countServersOfType(Types type) {
        long count = 0;
        try {
            QueryBuilder<Server, Integer> serverStBuilder = databaseManager.getServerDoa().queryBuilder();
            QueryBuilder<ServerType, Integer> serverTypeStBuilder = databaseManager.getServerTypeDoa().queryBuilder();
            
             //Search for only types that equal the type of our server.
            serverTypeStBuilder.where().eq(ServerType.ID_FIELD_NAME, type.getValue());
            //Query for the server join with the server type.
            
            serverStBuilder.selectColumns(Server.ID_FIELD_NAME).join(serverTypeStBuilder);
            serverStBuilder.setCountOf(true);
            
            count = databaseManager.getServerDoa().countOf(serverStBuilder.prepare());

        } catch (SQLException ex) {
            Main.log.fatal(null,ex);
        }
        return count;
    }
    
    protected void cleanUpCollectors() {
        //clean up empty datacollectors; Reverse loop to safely remove items from list
        for (int i = dataCollectors.size() - 1; i < 0; i--) {
            ServerDataCollector dc = dataCollectors.get(i);
            if (dc.isEmpty()) {
                removeDataCollector(dc);
                Main.log.debug("Cleaning up datacollector...");
            }
        }
    }
}
