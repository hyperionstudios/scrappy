/*
 * Copyright (C) 2014 Dylan.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package com.hyperionstudios.projectgxpbackend.managers;

import com.hyperionstudios.projectgxpbackend.Main;
import com.hyperionstudios.projectgxpbackend.entities.Vote;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Vote manager
 * 
 * Purpose: to purge all votes each month
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
public class VoteManager implements Runnable {
    
    private final int WAIT_TIME = 3600; //1 hour in seconds
    protected final Thread thread;
    
    protected DatabaseManager databaseManager;
    
    public VoteManager() {
        thread = new Thread(this);
        thread.setName("DCManager");
    }

    @Override
    public void run() {
        while (!thread.isInterrupted()) {
            
            if (hasMonthElapsed()) {
                //purge all votes
                try {
                    purgeVotes();
                }
                catch(SQLException ex) {
                    Main.log.error("Failed to purge votes", ex);
                }
            }
            
            try {
                //sleep for like an hour?
                Thread.sleep(WAIT_TIME*1000);
            }
            catch(InterruptedException ex) {
                thread.interrupt();
            }
        }
        Main.log.info("Stopping voteManager");
    }
    
    public void start() {
        thread.start();
    }
    
    public void stop() {
        thread.interrupt();
    }
    
    public Thread getThread() {
        return thread;
    }
    
    protected boolean hasMonthElapsed() {
        Calendar lastPurge = GregorianCalendar.getInstance();
        lastPurge.setTime(Main.config.getOptions().getLastVotePurge());

        Calendar now = GregorianCalendar.getInstance();

        int lastPurgeYear = lastPurge.get(Calendar.YEAR);
        int lastPurgeMonth = lastPurge.get(Calendar.MONTH);
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);

        int monthsElapsed = (year - lastPurgeYear) * 12 + (month - lastPurgeMonth);
        
        return monthsElapsed >= 1;
    }
    
    protected void purgeVotes() throws SQLException {
        Main.log.info("Monthly purging the votes...");
        databaseManager = new DatabaseManager();
        
        TableUtils.clearTable(databaseManager.getPooledConnectionSource(), Vote.class);
        
        databaseManager = null;
        
        Main.config.getOptions().setLastVotePurge(new Date());
        Main.config.Save();
    }
    
}
