/*
 * The MIT License
 *
 * Copyright (C) 2014 Pickle <curtisdhi@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hyperionstudios.projectgxpbackend;

import com.hyperionstudios.projectgxpbackend.managers.DataCollectorManager;
import com.hyperionstudios.projectgxpbackend.managers.VoteManager;
import java.util.Scanner;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


/**
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
public class Main 
{
    public final static String NAME = "ProjectGxp BackEnd";
    public final static Logger log = Logger.getLogger(Main.class);
       
    public final static Config config = new Config();
    
    public static DataCollectorManager dcManager;
    public static VoteManager voteManager;
    
    /**
     * 
     */
    
    public static void main(String[] args)
    {  
        log.log(Level.INFO, "Starting.... "+ Main.NAME);
        
        try {
            config.Load();
          
            dcManager = new DataCollectorManager();
            dcManager.start();
            
            voteManager = new VoteManager();
            voteManager.start();
            
            //simple input reader.
            Scanner input = new Scanner(System.in);
            boolean exit = false;
            
            while(!exit) {
                switch (input.next()) {
                    case "exit":
                        exit = true;
                        break;
                }

            }
            
            dcManager.stop();
            voteManager.stop();
            
            try {
                dcManager.getThread().join();
                voteManager.getThread().join();
            }
            catch (InterruptedException ex) {
                 Main.log.log(Level.FATAL, "A thread got interrupted! ", ex);
            }
            
            config.Save();
            
        }
        catch (Exception optional) {
            //Do not start if an exception is caught here.
        }
    }
    
}
