/*
 * The MIT License
 *
 * Copyright (C) 2014 Pickle <curtisdhi@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hyperionstudios.projectgxpbackend;

import java.util.Date;

/**
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
public class Options {
    private String sqlHost = "localhost";
    private short sqlPort = 3306;
    private String sqlDatabaseName;
    private String sqlUsername = "root";
    private String sqlPassword;
    private float serverUptimeThreshold = 0.20f;
    private int serversNewTimeSpanThreshold = 604800; //7 days in seconds
    
    private long serversPerThread = 1000L;
    private long dcManagerThreadMaxExecutionTime = 300000L; //5 minutes in milliseconds
    private long dcThreadMaxExecutionTime = 180000L; //3 minutes in milliseconds
    private Date lastVotePurge = new Date();
    
    public Options() { }
    
    /**
    * @return SQL hostname
    */
    public String getSqlHost() {
        return sqlHost;
    }
    
    /**
    * @return SQL network port
    */
    public short getSqlPort() {
        return sqlPort;
    }
    
    /**
    * @return SQL database name
    */
    public String getSqlDatabaseName() {
        return sqlDatabaseName;
    }
    
    /**
    * @return SQL username
    */
    public String getSqlUsername() {
        return sqlUsername;
    }
    
    /**
    * @return SQL password
    */
    public String getSqlPassword() {
        return sqlPassword;
    }
    
    /**
    * @return Server's uptime threshold
    */
    public float getServerUptimeThreshold() {
        return serverUptimeThreshold;
    }
    
    /**
    * @return Server's time span threshold in seconds to be considered new (recently added).
    */
    public int getServersNewTimeSpanThreshold() {
        return serversNewTimeSpanThreshold;
    }
    
    /** 
    * @return Servers to fetch and attach to a single thread
    */
    public long getServersPerThead() {
        return serversPerThread;
    }
    
    /** 
    * @return Data Collector Manager's max execution time in milliseconds
    */
    public long getDCManagerThreadMaxExecutionTime() {
        return dcManagerThreadMaxExecutionTime;
    }
    
    /** 
    * @return Data Collector's max execution time in milliseconds
    */
    public long getDCThreadMaxExecutionTime() {
        return dcThreadMaxExecutionTime;
    }
    
    /**
     * 
     * @return Last vote purge's date
     */
    public Date getLastVotePurge() {
        return lastVotePurge;
    }
    
    public void setLastVotePurge(Date lastVotePurge) {
        this.lastVotePurge = lastVotePurge;
    }
    
}
