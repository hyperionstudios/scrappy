/*
 * Copyright (C) 2014 Dylan.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package com.hyperionstudios.projectgxpbackend.daos;

import com.hyperionstudios.projectgxpbackend.entities.BannerImage;
import com.hyperionstudios.projectgxpbackend.entities.Server;
import com.hyperionstudios.projectgxpbackend.entities.ServerPostBack;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import java.sql.SQLException;

/**
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
public class ServerDaoImpl extends BaseDaoImpl<Server, Integer> implements ServerDao {
    private final Dao<ServerPostBack, Integer> postBackDao;
    private final Dao<BannerImage, Integer> bannerImageDao;
    
    public ServerDaoImpl(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Server.class);
        postBackDao = DaoManager.createDao(connectionSource, ServerPostBack.class);
        bannerImageDao = DaoManager.createDao(connectionSource, BannerImage.class);
    }
    
    @Override
    public int delete(Server server) throws SQLException {
        //delete all one to one relationships
        if (server.getPostBack() != null) {
            postBackDao.delete(server.getPostBack());
        }
        if (server.getBannerImage() != null) {
            bannerImageDao.delete(server.getBannerImage());
        } 
        return super.delete(server);
    }
}
