/*
 * The MIT License
 *
 * Copyright (C) 2014 Pickle <curtisdhi@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hyperionstudios.projectgxpbackend.entities;

import com.hyperionstudios.projectgxpbackend.Main;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;


/**
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
@DatabaseTable(tableName = "pgxp_servers")
public class Server {
     public static final String ID_FIELD_NAME = "id";
     public static final String TYPE_ID_FIELD_NAME = "type_id";
     public static final String NAME_FIELD_NAME = "name";
     public static final String IP_FIELD_NAME = "ip";
     public static final String PORT_FIELD_NAME = "port";
     public static final String CREATION_DATE_FIELD_NAME = "creationDate";
     public static final String LAST_PING_FIELD_NAME = "lastPing";
     public static final String ISONLINE_FIELD_NAME = "isOnline";
     public static final String UPTIME_FIELD_NAME = "uptime";
     public static final String DOWNTIME_FIELD_NAME = "downtime";
     public static final String PLAYERS_COUNT_FIELD_NAME = "playersCount";
     public static final String MAX_PLAYERS_COUNT_FIELD_NAME = "maxPlayersCount";
     public static final String ONLINE_PLAYERS_FIELD_NAME = "onlinePlayers";
     
     @DatabaseField(id = true)
     private int id;
     
     @DatabaseField(foreign = true)
     private ServerType type;
     
     @DatabaseField(foreign = true, columnDefinition="integer references pgxp_serverpostbacks(id) on delete cascade")
     private ServerPostBack postback;
     
     @DatabaseField(foreign = true, columnDefinition="integer references pgxp_banners(id) on delete cascade")
     private BannerImage bannerImage;
     
     @DatabaseField
     private String name;
     
     @DatabaseField
     private String ip;
     
     @DatabaseField
     private int port;

     @DatabaseField
     private String version;
     
     @DatabaseField
     private Date creationDate;
     
     @DatabaseField
     private Date lastPing;
     
     @DatabaseField
     private boolean isOnline;
     
     @DatabaseField
     private long uptime;
     
     @DatabaseField
     private long downtime;
          
     @DatabaseField
     private int playersCount;
     
     @DatabaseField
     private int maxPlayersCount;
     
     @DatabaseField
     private String apiKey;
     
     /*
     * NOTE: Because ormlite does not have a builtin "simple_array"
     * like doctrine2 does, we will build it ourselves.
     */
     @DatabaseField
     private String onlinePlayers;
     
     //@DatabaseField
     private int votes;
     
     
     public int getId() {
         return id;
     }
     
     public void setType(ServerType type) {
         this.type = type;
     }
     public ServerType getType() {
         return type;
     }
     
     public void setPostBack(ServerPostBack postback) {
         this.postback = postback;
     }
     public ServerPostBack getPostBack() {
         return postback;
     }
     
     public void setBannerImage(BannerImage bannerImage) {
         this.bannerImage = bannerImage;
     }
     public BannerImage getBannerImage() {
         return bannerImage;
     }
     
     public void setName(String name) {
         this.name = name;
     }
     
     public String getName() {
         return name;
     }
     
     public void setIp(String ip) {
         this.ip = ip;
     }
     public String getIp() {
         return ip;
     }
     
     public void setPort(int port) {
         this.port = port;
     }
     public int getPort() {
         return port;
     }
     
     public void setVersion(String version) {
         this.version = version;
     }
     public String getVersion() {
         return version;
     }
     
     public void setCreationDate(Date creationDate) {
         this.creationDate = creationDate;
     }
     public Date getCreationDate() {
         return creationDate;
     }
     
     public void setLastPing(Date lastPing) {
         this.lastPing = lastPing;
     }
     public Date getLastPing() {
         if (lastPing == null) {
             lastPing = new Date();
         }
         return lastPing;
     }
     
     public void setIsOnline(boolean isOnline) {
         this.isOnline = isOnline;
     }
     public boolean IsOnline() {
         return isOnline;
     }
     
     public void setUptime(long uptime) {
         this.uptime = uptime;
     }
     public long getUptime() {
         return uptime;
     }
     
     public void setDowntime(long downtime) {
         this.downtime = downtime;
     }
     public long getDowntime() {
         return downtime;
     }
     
    public void setApiKey(String apiKey) {
         this.apiKey = apiKey;
     }
     public String getApiKey() {
         return apiKey;
     } 
 
    public float getUptimePercentage() {
        /**
         Downtime = 10 tests
         Uptime = 15 tests

         Downtime (in %)
         = [10/30] x 100
         = 33.33%

         Uptime (in %)
         = 1 - 0.3333
         = 66.67%
        */
        float up = 1;
        float down;
        if (downtime != 0) {
            //cast the integers into floats to properly get a decimal
            down = (float)downtime / (float)(uptime +  downtime);
            up = 1 - down;
            //remove all the precision to 2 decimal places.
            NumberFormat formatter = new DecimalFormat("#0.00");
            up = Float.valueOf(formatter.format(up));
            
            if (up < 0) { up = 0; }
            else if (up > 1) { up = 1; }
        }
        /*
        $up = 0;
        $down = 0;
        if ($this->uptime) {
            $down = ($this->downtime / $this->uptime);
            $up = round(1 - $down,2);
            if ($up < 0) { $up = 0; }
            else if ($up > 1) { $up = 1; }
        }*/
        
        return up;
    }
     
     public void setPlayersCount(int playersCount) {
         this.playersCount = playersCount;
     }
     public int getPlayersCount() {
         return playersCount;
     }
          public void setMaxPlayersCount(int maxPlayersCount) {
         this.maxPlayersCount = maxPlayersCount;
     }
     public int getMaxPlayersCount() {
         return maxPlayersCount;
     }
     
     /* 
     * NOTE: this should be move into a ultity class in the future.
     * Here we take an array of strings and
     * bind them togather into a comma delimited
     * string for doctrine's "simple_array"
     */
     public void setOnlinePlayers(String[] players) {
         if (players == null) {
             this.onlinePlayers = null;
         }
         else {
            StringBuilder sb = new StringBuilder();
            for(String player : players) {
                sb.append(player);
                sb.append(",");
            } 
            //Remove the last comma added.
            this.onlinePlayers = sb.length() > 0 ? sb.substring(0, sb.length()-1) : "";
         }
     }
     /*
     * We break down the comma delimited string to a
     * string array.
     */
     public String[] getOnlinePlayers() {
         return onlinePlayers.split(",");
     }
     
     public void setVote(int votes) {
         this.votes = votes;
     }
     public int getVotes() {
         return votes;
     }
     
     
     /* Ultitlies functions */
     
     
     public boolean isNew() {
         //convert into seconds
         long diff = (new Date().getTime() - creationDate.getTime()) / 1000;

         return diff <= Main.config.getOptions().getServersNewTimeSpanThreshold();
     }
     /*
     *  Returns true if uptime is less than the acceptableUptime.
     */
     public boolean isBeyondAcceptableUptime(float acceptableUptime) { 
         return getUptimePercentage() < acceptableUptime; 
     }
     
}
