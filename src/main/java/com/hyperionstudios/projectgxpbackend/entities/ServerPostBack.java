/*
 * Copyright (C) 2014 Dylan.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package com.hyperionstudios.projectgxpbackend.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
@DatabaseTable(tableName = "pgxp_serverpostbacks")
public class ServerPostBack {
    /*
    * don't need any of the rest of the info atm...
    */
    @DatabaseField(id = true)
    private int id;
    
    public int getId() {
        return id;
    }
}
