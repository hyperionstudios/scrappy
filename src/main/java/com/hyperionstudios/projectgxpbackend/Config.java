/*
 * The MIT License
 *
 * Copyright (C) 2014 Pickle <curtisdhi@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hyperionstudios.projectgxpbackend;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


/**
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
public class Config {
    
    public static String CONFIG_PATH = "config/";
    public static String CONFIG_FILE = "config.json";
    
    private Options options;
    
    public void Load() throws FileNotFoundException {        
        File file = new File(CONFIG_PATH+CONFIG_FILE);
        if (!file.exists()) {
            Main.log.warn("Failed to load properties from "+CONFIG_PATH+CONFIG_FILE);
            //Failed to load, so load in defualts.
            Main.log.info("Using Defualts....  Please change the values.");
            Save();
            throw new FileNotFoundException();
        }
        
        Gson gson = new Gson();
        BufferedReader br = null; 
        try {
            br = new BufferedReader(new FileReader(file));
            options = gson.fromJson(br, Options.class);
        }
        catch (FileNotFoundException e) {
            Main.log.fatal(null,e);
        }
        finally {
            if (br != null) {
                try {
                    br.close();
                }
                catch (IOException e) {
                    Main.log.fatal(null,e);
                }
            }
        }
                
    }
    
    public void Save() {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        FileWriter fw = null;
        try {
            File dir = new File(CONFIG_PATH);
            dir.mkdirs();
            
            File file = new File(CONFIG_PATH+CONFIG_FILE);
            if (file.createNewFile()) {
                Main.log.info("Creating config file. Please change the values!");
            }
            
            fw = new FileWriter(file);
            if (options == null) {
                //load in the defaults.
                options = new Options();
            }
 
            fw.write(gson.toJson(options));
   
        }
        catch (IOException e) {
            Main.log.fatal("Failed to save config file", e);
        }
        finally {
            if (fw != null) {
                try {
                    fw.close();
                }
                catch (IOException e) {
                    Main.log.fatal(null, e);
                }
            }
        }
    }
    
    public Options getOptions() {
        return options;
    }
    
    
}
