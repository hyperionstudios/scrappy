/*
 * Copyright (C) 2014 Dylan.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package com.hyperionstudios.projectgxpbackend.dataCollectors.serverProbes;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
public class DefaultServerProbe implements IServerProbe {

    protected InetSocketAddress host;
    protected int timeout = 7000;

    public DefaultServerProbe(InetSocketAddress host, int timeout) {
        this.host = host;
        this.timeout = timeout;
    }

    public DefaultServerProbe(InetSocketAddress host) {
        this(host, 7000);
    }
    public DefaultServerProbe() { }
    
    @Override
    public void setAddress(InetSocketAddress host) {
        this.host = host;
    }

    @Override
    public InetSocketAddress getAddress() {
        return this.host;
    }

    @Override
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @Override
    public int getTimeout() {
        return this.timeout;
    }
    
    @Override
    public StatusResponse fetchData() throws IOException {
        StatusResponse response = null;
        try (Socket socket = new Socket()) {
            //don't need to read anything, just see if the server is online.
            socket.connect(host, timeout);
            
            response = new StatusResponse();        
            
        }
        catch (IOException optional) {
            throw optional;
        }
    
        return response;
    }
    
}