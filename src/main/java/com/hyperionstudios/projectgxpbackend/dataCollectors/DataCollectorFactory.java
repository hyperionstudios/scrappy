/*
 * The MIT License
 *
 * Copyright (C) 2014 Pickle <curtisdhi@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.hyperionstudios.projectgxpbackend.dataCollectors;

import com.hyperionstudios.projectgxpbackend.managers.DataCollectorManager;
import com.hyperionstudios.projectgxpbackend.Main;
import com.hyperionstudios.projectgxpbackend.entities.ServerTypes.Types;
import java.sql.SQLException;

/**
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
public class DataCollectorFactory {
    protected final DataCollectorManager manager;
    
    public DataCollectorFactory(DataCollectorManager manager) {
        this.manager = manager;
    }
    
    public ServerDataCollector createDataCollector(Types type, long limit, long page) {
        ServerDataCollector dataCollector = null;
        try {
            switch (type) {
                case MINECRAFT:
                    dataCollector = new MinecraftDataCollector(manager, limit, page);
                    break;
                case KERBAL_SPACE_PROGRAM:
                    dataCollector = new KSPDataCollector(manager, limit, page);
                    break;
                case TERRAIA:
                    dataCollector = new ServerDataCollector(manager, type, limit, page);
                    break;
                case STARBOUND:
                    dataCollector = new ServerDataCollector(manager, type, limit, page);
                    break;
            }
        }
        catch (SQLException ex) {
            Main.log.fatal("Unable to add datacollector.", ex);
        }
        return dataCollector;
    }
    
}
