/*
 * The MIT License
 *
 * Copyright (C) 2014 Pickle <curtisdhi@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.hyperionstudios.projectgxpbackend.dataCollectors;

import com.hyperionstudios.projectgxpbackend.managers.DataCollectorManager;
import com.hyperionstudios.projectgxpbackend.Main;
import com.hyperionstudios.projectgxpbackend.dataCollectors.serverProbes.DefaultServerProbe;
import com.hyperionstudios.projectgxpbackend.dataCollectors.serverProbes.IServerProbe;
import com.hyperionstudios.projectgxpbackend.dataCollectors.serverProbes.Player;
import com.hyperionstudios.projectgxpbackend.dataCollectors.serverProbes.StatusResponse;
import com.hyperionstudios.projectgxpbackend.entities.Server;
import com.hyperionstudios.projectgxpbackend.entities.ServerType;
import com.hyperionstudios.projectgxpbackend.entities.ServerTypes.Types;
import com.j256.ormlite.stmt.QueryBuilder;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Pickle <curtisdhi@gmail.com>
 */
public class ServerDataCollector implements Runnable {

    protected final DataCollectorManager manager;
    protected final Types type;
    protected final long sqlLimit;
    protected final long page;

    protected Thread thread;
    protected List<Server> servers;

    protected long maxExecutionTime = 180000L; //3 minutes

    protected boolean isEmpty;

    public ServerDataCollector(DataCollectorManager manager, Types type, long sqlLimit, long page) {

        this.manager = manager;
        this.type = type;
        this.page = page;
        this.sqlLimit = sqlLimit;
        this.servers = new ArrayList<>();

        this.maxExecutionTime = Main.config.getOptions().getDCThreadMaxExecutionTime();

        thread = new Thread(this);
        thread.setName(type.name() + "_" + page);

    }

    public ServerDataCollector(DataCollectorManager manager, Types type, long page) {
        this(manager, type, 250L, page);
    }

    @Override
    public void run() {
        while (!thread.isInterrupted()) {
            Main.log.debug("Collecting data for " + type.name() + ", page " + page);
            Date time = new Date();

            try {
                collectData();
            } catch (SQLException ex) {
                Main.log.error("Failed to collect data on " + getType().toString() + " collector:", ex);
            }

            //for debugging and benchmarking
            final int serversCount = servers.size();

            //release this object to be garbage collected.
            servers = null;

            //Let this thread to sleep if this took less than the time limit.
            long diffTime = new Date().getTime() - time.getTime();
            Main.log.debug((diffTime / 1000) + " seconds to collect data for " + serversCount + " servers");

            long sleepTime = maxExecutionTime - diffTime;
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException ex) {
                //Main.log.error(null, ex);
                thread.interrupt();
            }

        }
        Main.log.debug("Stopping data collecter " + type.name() + ", page " + page);

    }

    /*
     *   Purpose of this method is to search for a spefic amount of servers of a spefic type
     *   And ping their online status.
     */
    public void collectData() throws SQLException {

        QueryBuilder<Server, Integer> serverStBuilder = manager.getDBManager().getServerDoa().queryBuilder();
        QueryBuilder<ServerType, Integer> serverTypeStBuilder = manager.getDBManager().
                getServerTypeDoa().queryBuilder();

        //Search for only types that equal the type of our server.
        serverTypeStBuilder.where().eq(ServerType.ID_FIELD_NAME, type.getValue());
        //Query for the server join with the server type.
        serverStBuilder.limit(sqlLimit).offset(page * sqlLimit).join(serverTypeStBuilder);

        servers = manager.getDBManager().getServerDoa().query(serverStBuilder.prepare());

        //if we have no servers, flag this datacollector empty, so the manager may destroy it.
        if (flagAsEmpty()) {
            return;
        }

        /*
         * Reverse loop so we can safely remove items from the list.
         */
        for (int i = servers.size() - 1; i >= 0; i--) {
            Server server = servers.get(i);
            
            //Collect data before testing to remove it?
            Main.log.debug("Collecting data for server: " + server.getId() + " uptime: " + server.getUptimePercentage());
            collectServerData(server);
                
            //If server is beyond our uptime limit and isn't new, time to discard it.
            //make sure we have pinged it atleast once (in the event our service failed and the server is no longer new)
            if (server.getLastPing() != null && !server.isNew() && server.isBeyondAcceptableUptime(getAcceptableUptime())) {
                Main.log.info("Removing server: " + server.getId() + " uptime: " + server.getUptimePercentage());
                //purge server
                removeServer(server);
            }
            
        }

    }

    public void collectServerData(Server server) {
        StatusResponse sr = pingServer(server);

        //if sr is null, than the server is offline.
        Date lastPing = new Date();

        server.setPlayersCount(0);
        //server.setMaxPlayersCount(0);
        server.setOnlinePlayers(null);
        if (sr != null) {
            server.setIsOnline(true);

            if (sr.getPlayers() != null) {
                server.setPlayersCount(sr.getPlayers().getOnline());
                server.setMaxPlayersCount(sr.getPlayers().getMax());
                List<Player> players = sr.getPlayers().getSample();

                if (players != null) {
                    String[] strPlayers = new String[players.size()];
                    for (int i = 0; i < strPlayers.length; i++) {
                        strPlayers[i] = players.get(i).getName();
                    }
                    server.setOnlinePlayers(strPlayers);
                }
            }

            server.setVersion(sr.getVersion().getName());

            //Add uptime as the server is online.
            long uptime = server.getUptime() + 1;
            server.setUptime(uptime);
        } else {
            //Add downtime as the server is offline.
            long downtime = server.getDowntime() + 1;
            server.setDowntime(downtime);
            server.setIsOnline(false);
        }

        server.setLastPing(lastPing);

        //update the entry in the db here.
        try {
            manager.getDBManager().getServerDoa().update(server);
        } catch (SQLException ex) {
            Main.log.error("Unable to update server " + server.getId(), ex);
        }
    }

    public StatusResponse pingServer(Server server) {
        DefaultServerProbe probe = new DefaultServerProbe(
                new InetSocketAddress(server.getIp(), server.getPort())
        );
        StatusResponse sr = pingServer(server, probe);
        return sr;
    }

    public StatusResponse pingServer(Server server, IServerProbe probe) {
        StatusResponse sr = null;

        try {
            if (probe != null) {
                sr = probe.fetchData();
            }
        } catch (IOException ex) {
            //Not a severe exception because we are expecting to not connect (server might be offline)
            Main.log.debug("Unable to fetch data from server id: " + server.getId());
        }
        return sr;
    }

    public float getAcceptableUptime() {
        return Main.config.getOptions().getServerUptimeThreshold();
    }

    /** 
     * Flags data collector as empty if page is not 0 and server list is empty.
     * 
     * @return boolean
     */
    protected boolean flagAsEmpty() {
        //We must maintain atleast one data collector for each type, 
        //so we won't be generating garbage all the time.
        if (page != 0 && servers.isEmpty()) {
            isEmpty = true;
        }
        isEmpty = false;
        return isEmpty;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public Types getType() {
        return type;
    }

    public Thread getThread() {
        return thread;
    }

    public List<Server> getServers() {
        return servers;
    }

    public void addServer(Server server) {
        servers.add(server);
    }

    public void removeServer(Server server) {
        servers.remove(server);
        try {
            manager.getDBManager().getServerDoa().delete(server);
        } catch (SQLException ex) {
            Main.log.error("Failed to delete Server: " + server.getId(), ex);
        }
    }

}
